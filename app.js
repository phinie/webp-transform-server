var express = require('express');
var CWebp = require('cwebp').CWebp;

var app = express();

app.route('/')
	.get(function (req, resp) {
		resp.send("Simple Webp image transformer. Just POST image to / and receive the webp encoded file as answer.");
	})
	.post(function(req,resp) {
		resp.type('image/webp');
		new CWebp(req).stream().pipe(resp);
	});

app.listen(8080);